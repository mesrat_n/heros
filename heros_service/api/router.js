'use strict';

const heroController = require('./controllers/herosController');
const defaultController = require('./controllers/defaultController');

module.exports = function (app) {

	/**
	 * All heros - Get all heros
	 */
	app.route('/')
		.get(defaultController.getResponse);
	
	app.route('/heros')
		.get(heroController.getAll);
	
	app.route('/heros/:id')
		.get(heroController.getHero);

	app.route('/heros')
		.post(heroController.createHero);

	app.route('/heros/:id')
		.put(heroController.updateHero);

	
	app.use((err, req, res, next) => {
		if (err) {
			if (err.httpCode)
				res.status(err.httpCode).send({ error: err.error });
			else
				res.status(520).send({ error: err + "" });
		} else {
			res.status(404).send({ error: req.originalUrl + ' not found' });
		}
	});
}
