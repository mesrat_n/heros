//Require the dev-dependencies
const chai = require('chai'),
    chaiHttp = require('chai-http'),
    server = require('../server'),
    should = chai.should();

// Use http requests
chai.use(chaiHttp);

// Required testers
const expect = chai.expect;

const create_hero = require('./create_hero');
const update_hero = require('./update_hero');


describe('Heros', function () {
    this.timeout(30000);

    beforeEach((done) => {
        //Before each test if needed
        done();
    });

    describe('/GET heros', () => {
        it('it should gather informations on 2 heros', (done) => {
            chai.request(server)
                .get('/heros')
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('object');
                    res.body.heros.should.be.a('array');
                    expect(res.body.heros.length).to.not.equal(0);
                    expect(res.body.heros.length).to.be.equal(2);

                    // Check heros reponse values
                    expect(res.body.heros[0].gender).to.be.equal("Male");
                    expect(res.body.heros[0].superpower).to.be.equal("Happiness");
                    expect(res.body.heros[0].name).to.be.equal("Merlin");

                    expect(res.body.heros[1].gender).to.be.equal("Male");
                    expect(res.body.heros[1].superpower).to.be.equal("Love");
                    expect(res.body.heros[1].name).to.be.equal("Neo");

                    done();
                });
        });
    });

    describe('/GET hero', () => {
        it('it should gather informations on 1 hero', (done) => {
            chai.request(server)
                .get('/heros/1')
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('object');
                    res.body.heros.should.be.a('array');
                    expect(res.body.heros.length).to.not.equal(0);
                    expect(res.body.heros.length).to.be.equal(1);

                    // Check heros reponse values
                    expect(res.body.heros[0].gender).to.be.equal("Male");
                    expect(res.body.heros[0].superpower).to.be.equal("Happiness");
                    expect(res.body.heros[0].name).to.be.equal("Merlin");

                    done();
                });
        });
    });

    describe('/GET hero', () => {
        it('it should not gather any information', (done) => {
            chai.request(server)
                .get('/heros/10')
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(404);
                    expect(err).to.be.not.null;
                    
                    // Check response type and length
                    res.body.should.be.a('object');
                    expect(res.body.heros.error).to.be.equal('No hero found');

                    done();
                });
        });
    });

    describe('/POST hero', () => {
        it('it should create a new hero', (done) => {
            chai.request(server)
                .post('/heros')
                .send(create_hero)
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('string');
                    expect(res.body).to.be.equal('Hero created');

                    done();
                });
        });
    });

    describe('/PUT hero', () => {
        it('it should update a hero', (done) => {
            chai.request(server)
                .put('/heros/3')
                .send(update_hero)
                .end((err, res) => {
                    // Check errors and response http code
                    res.should.have.status(200);
                    expect(err).to.be.null;
                    
                    // Check response type and length
                    res.body.should.be.a('string');
                    expect(res.body).to.be.equal('Hero updated');

                    done();
                });
        });
    });
});
