const errorsConstants = require('../constants/errors');

exports.getAll = async (req, res) => {
    let query = "SELECT * FROM `heros` ORDER BY id ASC";

    // execute query
    db.query(query, (err, result) => {
        if (err) {
            res.status(400).json({
                heros: errorsConstants.MYSQL_ERROR
            });
        }
        if (result.length < 1) {
            res.status(404).json({
                heros: errorsConstants.NO_HERO
            });
        }
        res.json({
            heros: result
        });
    });
}

exports.getHero = async (req, res) => {
    let id = req.params.id;
    let query = "SELECT * FROM `heros` WHERE id = "+id;

    // execute query
    db.query(query, (err, result) => {
        if (err) {
            res.status(400).json({
                heros: errorsConstants.MYSQL_ERROR
            });
        }
        if (result.length < 1) {
            res.status(404).json({
                heros: errorsConstants.NO_HERO
            });
        } else { 
            res.json({
                heros: result
            });
        }
    });
}

exports.createHero = async (req, res) => {
    let name       = req.body.hero.name;
    let gender     = req.body.hero.gender;
    let superpower = req.body.hero.superpower;

    let query = "INSERT INTO `heros` (name, gender, superpower)"+
                "VALUES ('"+name+"', '"+gender+"', '"+superpower+"')";

    // execute query
    db.query(query, (err, result) => {
        if (err) {
            res.status(400).json({
                heros: errorsConstants.MYSQL_ERROR
            });
        }
        res.json("Hero created");
    });
}

exports.updateHero = async (req, res) => {
    let id = req.params.id;

    let name       = req.body.hero.name;
    let gender     = req.body.hero.gender;
    let superpower = req.body.hero.superpower;

    let query = "UPDATE `heros` SET name = '"+name+"', gender = '"+gender+"', superpower = '"+superpower+"' "
                +" WHERE id = "+id;

    // execute query
    db.query(query, (err, result) => {
        if (err) {
            console.log("in error");
            res.status(400).json({
                heros: errorsConstants.MYSQL_ERROR
            });
        }
        res.json("Hero updated");
    });
}