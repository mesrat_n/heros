// Load modules
const express = require('express'),
    app = express(),
    port = process.env.PORT || 8080,
    morgan = require('morgan'),
    bodyParser = require('body-parser')
    mysql = require('mysql');

// Config DB
const db = mysql.createConnection ({
    host: process.env.HEROS_DB_HOST || 'localhost',
    user: 'root',
    password: process.env.MYSQL_ROOT_PASSWORD || 'MerlinPinPin',
    database: process.env.DATABASE_NAME || 'Heros'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;


// Config API
if (process.env.NODE_ENV !== 'test')
    app.use(morgan('combined'))

app.use(bodyParser.json({
    limit: '50mb',
    extended: true
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
}));


// Load routes & 404
var routes = require('./api/router'); //importing routes
routes(app); //register the route

// Start API
app.listen(port, () => {
    console.log(`Heros RESTful API server started on ${port}`)
  })


// Export app for testings
module.exports = app;
