CREATE DATABASE IF EXISTS Heros;

USE Heros

# Dump of table heros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `heros`;

CREATE TABLE `heros` (
  `id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `gender` VARCHAR(100) NOT NULL,
  `superpower` VARCHAR(255) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `heros` WRITE;
/*!40000 ALTER TABLE `heros` DISABLE KEYS */;

INSERT INTO `heros` (`id`, `name`, `gender`, `superpower`)
VALUES
	(1, "Merlin", "Male", "Happiness"),
	(2, "Neo", "Male", "Love");

/*!40000 ALTER TABLE `heros` ENABLE KEYS */;
UNLOCK TABLES;
