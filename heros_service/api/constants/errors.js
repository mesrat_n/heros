module.exports = {
    UNHANDLED_ERROR: {
        code: -1,
        error: '',
        httpCode: 520
    },
    NO_HERO: {
        code: 20,
        error: 'No hero found',
        httpCode: 404
    },
    MYSQL_ERROR: {
        code: 20,
        error: 'MYSQL ERROR',
        httpCode: 400
    }
}
